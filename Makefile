CC=gcc
CFLAGS=-Wall -pedantic -Werror -g
BIN=snake_game.exe

sys=system
ser=server

OBJ=$(sys)/proc.o $(sys)/inet.o $(sys)/game.o main.o

all: client serv

main.o: main.c
	$(CC) $(CFLAGS) -c -o $@ $<

client: main.o
	make -C $(sys)
	$(CC) $(CFLAGS) -lncurses -o $(BIN) $(OBJ)

serv:
	make -C $(ser)

clean:
	make -C $(sys) clean
	make clean -C $(ser)
	rm -rf *.o *.exe

#include "linklist.h"

node_t *insert_node(node_t *list, void *data)
{
    node_t *lk = malloc(sizeof(node_t));
    lk->data = data;
    lk->next = list->next;
    lk->prev = list;
    list->next = lk;
    
    if (lk->next != NULL)
        lk->next->prev = lk;
    
    return lk;
}


void delete_node(node_t *list, node_t **root, node_t **last)
{
    if (list->prev == NULL)
    {
        list->next->prev = NULL;
        *root = list->next;
    }
    else if (list->next == NULL)
    {
        list->prev->next = NULL;
        *last = list->prev;
    }
    else
    {
        list->next->prev = list->prev;
        list->prev->next = list->next;
    }

    free(list->data);
    list->data = NULL;

    free(list);
    list = NULL;
}

#ifndef LINKLIST_H
#define LINKLIST_H

#include "../../system/game.h"

typedef struct node
{
    void *data;
    struct node *next;
    struct node *prev;
} node_t;

node_t *insert_node(node_t *afternode, void *data);

void delete_node(node_t *node, node_t **root, node_t **last);

#endif

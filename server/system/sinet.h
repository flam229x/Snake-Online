#ifndef SINET_H
#define SINET_H

#include <arpa/inet.h>
#include <sys/select.h>

#include "../../system/proc.h"
#include "sgame.h"

void init_server(const struct sockaddr_in *addr);

void wait_clients();

void send_init_mess(const struct timespec *timesp, uint16_t fr_d, 
        uint16_t fsx, uint16_t fsy);

void send_init_fruit(uint16_t fx, uint16_t fy);

void send_alive_snakes(int fd);

uint16_t fd2num(uint16_t fd);

void send_snakes();

void dead(uint16_t num);

void send_fruit();

#endif

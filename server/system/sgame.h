#ifndef SSYSTEM_H
#define SSYSTEM_H

#include <inttypes.h>
#include <string.h>

#include "../../system/game.h"
#include "linklist.h"

#define SNAKE_SIZE 3

void gen_snakes(uint16_t field_x, uint16_t field_y);

void change_crd(uint16_t num);

void change_crd_snakes();

int check_dead(uint16_t num, uint16_t fx, uint16_t fy);

void spawn_fruit(uint16_t fx, uint16_t fy);

int check_eat(uint16_t num);

#endif


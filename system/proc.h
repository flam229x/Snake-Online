#ifndef PROC_H
#define PROC_H

#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <time.h>
#include <sys/select.h>
#include <unistd.h>

#define HELP_CLIENT """-h \t Print help mess\n"""\
"""---------CLIENT----------\n"""\
"""-p \t Port (by default 2229)\n"""\
"""-a \t Address of server (by default localhost)\n"""

#define HELP_SERVER """-h \t Print help mess\n"""\
"""----------SERVER----------\n"""\
"""-s \t Server mode enable\n"""\
"""-x \t Size of field according to x (by default 80)\n"""\
"""-y \t Size of field according to y (by default 20)\n"""\
"""-d \t Frame delay mseconds (by default 300)\n"""\
"""-t \t Start time seconds (by default 1)\n"""\
"""-n \t Number of clients (by default 1)\n"""\
"""-p \t Port (by default 2229)\n"""\
"""-f \t Path to log file (by default stdin)\n"""



int Socket(int domain, int type, int protocol); 

void Connect(int socket, const struct sockaddr *address,
           socklen_t address_len);

in_addr_t Inet_addr(const char *cp);

ssize_t Recv(int socket, void *buffer, size_t length, int flags);

ssize_t Send(int socket, const void *buffer, size_t length, int flags);

pid_t Fork(void);

int Bind(int socket, const struct sockaddr *address,
           socklen_t address_len);

int Listen(int socket, int backlog);

int Accept(int socket, struct sockaddr *address,
   socklen_t *address_len);

void Pselect(int nfds, fd_set *readfs, const struct timespec *timeout);

struct timespec timespec_subtract(struct timespec start, struct timespec end);

#endif

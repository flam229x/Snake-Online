#ifndef INET_H
#define INET_H

#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

void connect_to_server(const struct sockaddr_in *addr);

void get_players();

void get_object();

void get_fruit();

void is_dead();

// void send_info_server(void *buf, size_t n);

#endif

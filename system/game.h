#ifndef SYSTEM_H
#define SYSTEM_H

#include <getopt.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <ncurses.h>
#include <time.h>

#define byte char

#define MAX_PLAYERS 255
#define MAX_SNAKE_SIZE 1024
#define MAX_FRUIT 5

#define STD_CHAR 'o'
#define FRUIT_CHAR '%'

#define PLAYER_PAIR 1

#define MOVE_UP 0x0
#define MOVE_DOWN 0x1
#define MOVE_RIGHT 0x2
#define MOVE_LEFT 0x3

#define MAX(a, b) ((a) > (b)) ? (a) : (b)

typedef struct crd
{
    uint16_t x;
    uint16_t y;

} __attribute__((packed)) crd_t;

typedef struct snake
{
    uint8_t dir;
    uint16_t len;
    crd_t body[MAX_SNAKE_SIZE];
} __attribute__((packed)) snake_t; 

typedef struct init_mess
{
    uint8_t magic;
    struct timespec timestamp;
    uint16_t frame_delay;
    uint16_t field_size_x;
    uint16_t field_size_y;
    uint16_t n_players;
    uint16_t my_number;
} __attribute__((packed)) init_mess_t;


void parse_arg(int argc, char **argv);

void draw_field(char chr);

void draw_snake(char chr, uint8_t num);

void draw_snakes();

#endif



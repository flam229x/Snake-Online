#include "game.h"
#include "proc.h"

init_mess_t init;
snake_t players[MAX_PLAYERS];
crd_t fruit;

extern uint8_t is_alive[MAX_PLAYERS];

void draw_field(char chr)
{
    for (int i = 0; i < init.field_size_x; ++i)
    {
        mvaddch(0, i, chr); 
        mvaddch(init.field_size_y-1, i, '#');
    }

    for (int i = 0; i < init.field_size_y; ++i)
    {
        mvaddch(i, 0, chr);
        mvaddch(i, init.field_size_x-1, '#');
    }

    refresh();
}


void draw_snake(char chr, uint8_t num)
{
    for (int i = 0; i < players[num].len; ++i)
    {
        mvaddch(players[num].body[i].y, players[num].body[i].x, chr); 
    }
}


void draw_snakes()
{
    for (int i = 0; i < init.n_players; ++i)
    {
        if (!is_alive[i]) continue;

        if (i == init.my_number)
        {
            attron(COLOR_PAIR(PLAYER_PAIR));
            draw_snake(STD_CHAR, i);
            attroff(COLOR_PAIR(PLAYER_PAIR));
        }
        else
            draw_snake(STD_CHAR, i);
    }
}


